package com.d3ti.pbolanjt20.thread;

public class Method_Thread extends Thread {
	
		public Method_Thread (String name) {
			super(name);
		}
		
		public static synchronized void tampil (String nama) {
			for(int i=1;i<6;i++) {
				try {
					Thread.sleep(300);
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("Thread: "+nama+" Posisi: "+i);
			}
		}
		
		public void run() {
			tampil(getName());
		}

		public static void main (String args[]) {
			Thread l1 = new Thread(new Method_Thread("l1"));
			Thread l3 = new Thread(new Method_Thread("l3"));
			l1.start();
			l3.start();
			
			Thread l2 = new Thread(new ThreadInterface("l2"));
			Thread l4 = new Thread(new ThreadInterface("l4"));
			l2.start();
			try {
				l2.join(300);
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
			l4.start();
		}
}
